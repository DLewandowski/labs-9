package pk.labs.Lab9.beans.impl;

import java.beans.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

public class ConsultationListImpl extends java.lang.Object implements ConsultationList, Serializable, VetoableChangeListener 
{ 
    public List<Consultation> consultations = new LinkedList<Consultation>();
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    public int rozmiarlisty = 0;

    @Override
    public Consultation[] getConsultation() 
    {
        return (consultations.toArray(new Consultation[consultations.size()]));
    }
    public void setConsultation(List consultations) 
    {
        this.consultations = consultations;
    }

    @Override
    public Consultation getConsultation(int index) 
    {
        return this.getConsultation()[index];
    }
    
    @Override
    public int getSize() 
    {
        return this.consultations.size();
    }
    
    public ConsultationListImpl() 
    {
        pcs = new PropertyChangeSupport(this);
        consultations = new LinkedList<Consultation>();
    }

    public ConsultationListImpl(List<Consultation> consultations) 
    {
        pcs = new PropertyChangeSupport(this);
        this.consultations = consultations;
    }

    public ConsultationListImpl(LinkedList<Consultation> consultations)
    {
        pcs = new PropertyChangeSupport(this);
        this.consultations = new LinkedList<Consultation>();
        try
        {
            for (Consultation consultation: consultations)
            {
                this.addConsultation(consultation);
            }
        } catch (PropertyVetoException ex)
        {
            ex.getMessage();
        }
    }
     public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException 
    {
        ConsultationImpl stara = (ConsultationImpl)event.getOldValue();
        long rozpocznijkons;
        rozpocznijkons = stara.getTerm().getBegin().getTime();
        int minuty = (Integer)event.getNewValue();
        int godzina = (Integer) event.getNewValue();
        long czas;
        czas = stara.getTerm().getEnd().getTime();
        czas = stara.getTerm().getEnd().getTime() + minuty * 60000;

        for (Consultation consultation : consultations) 
        {
            long zacznij = ((ConsultationImpl)consultation).getTerm().getBegin().getTime();
            if((rozpocznijkons < zacznij) && (czas >= zacznij)) 
            {
                throw new PropertyVetoException("duration", event);
            }
        }
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException 
    {
        try{
            rozmiarlisty = this.getSize();
            if (rozmiarlisty < 0) 
            {
                rozmiarlisty = this.getSize();
            }
            if (rozmiarlisty > 0) {
                vcs.addVetoableChangeListener((VetoableChangeListener)consultation);
                vcs.fireVetoableChange("consultation", consultations.get(getSize() - 1), consultation);
            }
            ((ConsultationImpl)consultation).addVetoableChangeListener(this);
            consultations.add(consultation);

            pcs.firePropertyChange("consultation", rozmiarlisty, rozmiarlisty + 1);

        } 
        catch(PropertyVetoException e) 
        {
            throw e;
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) 
    {
        pcs.addPropertyChangeListener(listener);
    }
    
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) 
    {
        pcs.removePropertyChangeListener(listener);
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) 
    {
        vcs.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) 
    {
        vcs.removeVetoableChangeListener(listener);
    }
    
    

   
}
