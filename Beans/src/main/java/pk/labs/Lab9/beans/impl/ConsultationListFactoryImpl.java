package pk.labs.Lab9.beans.impl;

import java.io.*;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.util.LinkedList;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

public class ConsultationListFactoryImpl implements ConsultationListFactory
{
    
    @Override
    public ConsultationList create() 
    {
        return new ConsultationListImpl();
    }
    
     @Override
    public void save(ConsultationList consultationList) 
    {       
        try 
        {
            LinkedList<Consultation> serializationList;
            serializationList = new LinkedList<Consultation>();
            for(Consultation consultation: consultationList.getConsultation())
            {
                serializationList.add(consultation);
            }
            FileOutputStream ouput = new FileOutputStream("consultations.xml");
            BufferedOutputStream aoubuffs = new BufferedOutputStream(ouput);
            XMLEncoder encoder = new XMLEncoder(aoubuffs);
            encoder.writeObject(serializationList);
            encoder.close();
        } catch (FileNotFoundException ex) 
        {
            ex.getMessage();
        }
    }

    @Override
    public ConsultationList create(boolean deserialize) 
    {
        if(deserialize){
            try
            {
                LinkedList<Consultation> listades;
                FileInputStream input = new FileInputStream("consultations.xml");
                BufferedInputStream inbuffs = new BufferedInputStream(input);
                XMLDecoder dekoder = new XMLDecoder(inbuffs);
                listades = (LinkedList<Consultation>) dekoder.readObject();
                return new ConsultationListImpl(listades);
            }
            catch(FileNotFoundException ex) 
            {
                ex.getMessage();
            }
        }
        return this.create();
    }

   
}
