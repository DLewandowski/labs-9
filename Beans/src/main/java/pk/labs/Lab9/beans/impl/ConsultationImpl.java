package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Date;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

public class ConsultationImpl extends Object implements Serializable, Consultation, VetoableChangeListener 
{

    public String osoba;
    private Term term;
    public ConsultationList lista;
    public PropertyChangeSupport pcs; 
    private final VetoableChangeSupport vcs = new VetoableChangeSupport(this);
    
    public void setStudent(String osoba) 
    {
        this.osoba = osoba;
    }
    
    @Override
    public String getStudent() 
    {
        return this.osoba;
    }
    
    @Override
    public void setTerm(Term term) throws PropertyVetoException
    {
        this.term = term;
    }
    
    public Term getTerm() 
    {
        return term;
    }
    
    @Override
    public Date getBeginDate() 
    {
        return this.term.getBegin();
    }
    
    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }
    
    
    public ConsultationImpl() {
        osoba = new String("");
        term = new TermImpl();
        pcs = new PropertyChangeSupport(this);     
    }

    public ConsultationImpl(String osoba, TermImpl term) {
        this.osoba = osoba;
        this.term = term;
        pcs = new PropertyChangeSupport(this);    
    }
    
    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException 
    {
   
        ConsultationImpl stara = (ConsultationImpl)event.getOldValue();
        long stara_p = stara.term.getBegin().getTime();
        long starak = stara.term.getEnd().getTime();
        
        ConsultationImpl nowa = (ConsultationImpl)event.getNewValue();
        long nowa_p = nowa.term.getBegin().getTime();
        long nowa_k = nowa.term.getEnd().getTime();
        
        if(stara_p >= nowa_p && nowa_k >= stara_p) 
        {
            throw new PropertyVetoException("consultation", event);
        }
//        if(stara_p <= nowa_p || starak  > nowa_p) 
//        {
//            throw new PropertyVetoException("consultation", event);
//        }
//        if(stara_k <= nowa_p && starak  >= nowa_k) 
//        {
//            throw new PropertyVetoException("consultation", event);
//        }
        if(stara_p <= nowa_p && starak  >= nowa_p) 
        {
            throw new PropertyVetoException("consultation", event);
        }
       
    }
    
    @Override
    public void prolong(int minutes) throws PropertyVetoException 
    {
        if(minutes > 0) 
        {
            try {
                vcs.fireVetoableChange("duration", this, minutes);
                this.term.setDuration(this.term.getDuration() + minutes);
            }
            catch(PropertyVetoException e) {
                throw e;
            }
        }
        if (minutes <= 0)
        {
            Term term_old = term;
            PropertyChangeEvent ivent = new PropertyChangeEvent(this, "term",term_old, term);
            
                if (lista != null)
            {
                for (Consultation c : lista.getConsultation())
                {
                    if (c == this)
                        continue;
                    // duplicating code...
                    // 1. before (we can check only consultation.endDate since it can't be before consultation.beginDate, but just in case...)
                    boolean not_collides = this.getBeginDate().before(c.getBeginDate()) && this.getEndDate().before(c.getBeginDate());
                    // 2. after (same here but reverse)
                    not_collides |= this.getBeginDate().after(c.getEndDate()) && this.getEndDate().after(c.getEndDate());
    
                    if (!not_collides)
                    {
                        // restore old term
                        term = term_old;
                        throw new PropertyVetoException("2 konsultacje sie nakładają po zmianie terminu", ivent);
                    }
                }
            }           
        }
                
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) 
    {
        pcs.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) 
    {
        pcs.removePropertyChangeListener(listener);
    }
 
    public void addVetoableChangeListener(VetoableChangeListener listener) 
    {
        vcs.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) 
    {
        vcs.removeVetoableChangeListener(listener);
    }  
}

